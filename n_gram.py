# N_GRAM.PY
#
# Contains class NGram used to perform calculations for n-grams
#   (averages, entropy, standard deviation for n-gram frequencies)
# Contains class Chart to generate charts for data from NGram class
#   (bar, pie, dendrograms)
from os import path, pathconf
from itertools import product
from math import log2
from statistics import mean, pstdev
from plotly import figure_factory
from scipy.spatial.distance import pdist
from csv import DictReader
from difflib import get_close_matches
import numpy as np
import sys
import matplotlib.pyplot as plt


class NGram:
    def __init__(self, all_data={}, entropy_values={}, entropy_average=None, entropy_standard_deviation=None,
                 frequency_averages={}, frequency_standard_deviations={}, frequency_standard_error={}, n_grams=[], n=None):
        """
        Constructor for class NGram, which keeps track of a set of NGramFile objects and performs calculations on each
        of the NGramFile objects.
        :param alphabet_file: shared alphabet file for NGramFile objects.
        :param n: length of the 'n'-gram.
        """
        self.all_data = all_data  # Keys are file names, with values being another dictionary which has n-gram as keys and frequencies as values.

        # Entropy variables
        self.entropy_values = entropy_values  # n-gram entropy for each key=file.
        self.entropy_average = entropy_average  # average n-gram entropy across files.
        self.entropy_standard_deviation = entropy_standard_deviation  # standard deviation for n-gram entropy across files.
        # Frequency variables
        self.frequency_averages = frequency_averages  # keys=n-gram strings, values=average frequencies
        self.frequency_standard_deviations = frequency_standard_deviations  # keys=n-gram strings, values=standard deviation across files
        self.frequency_standard_error = frequency_standard_error

        self.n = n
        self.n_grams = n_grams  # when itertools.product is called, it generates an n-length iterable instead of a string.

    @classmethod
    def n_gram_from_csv(cls, all_data_file, entropies_file=None):
        """
        Generates an n-gram object from a CSV file.
        :param all_data_file: the path to the csv file that is generated from n_gram_to_csv
        :entropies_file: similarly, the entropies file.
        """

        all_data = {}
        n_grams = []
        entropy_values = {}
        entropy_average = None
        entropy_standard_deviation = None
        frequency_averages = {}
        frequency_standard_deviations = {}
        frequency_standard_error = {}

        with open(all_data_file, "r") as f:
            parser = DictReader(f)
            for row in parser:
                for key in row:
                    if key == "n-gram":
                        n_grams.append(row[key])
                    elif key == "Average Frequency":
                        frequency_averages[n_grams[-1]] = float(row[key])
                    elif key == "Frequency Standard Deviation":
                        frequency_standard_deviations[n_grams[-1]] = float(row[key])
                    elif key == "Frequency Standard Error":
                        frequency_standard_error[n_grams[-1]] = float(row[key])
                    else:
                        if key not in all_data:
                            all_data[key] = {n_grams[-1]: float(row[key])}
                        else:
                            all_data[key][n_grams[-1]] = float(row[key])

        if entropies_file is not None:
            with open(entropies_file, "r") as f:
                parser = DictReader(f)
                for row in parser:
                    for key in row:
                        if key == "Average Entropy":
                            entropy_average = row[key]
                        elif key == "Entropy Standard Deviation":
                            entropy_standard_deviation = row[key]
                        else:
                            entropy_values[key] = row[key]

        return NGram(all_data, entropy_values, entropy_average, entropy_standard_deviation,
                     frequency_averages, frequency_standard_deviations, frequency_standard_error, n_grams, len(n_grams[0]))

    @classmethod
    def n_gram_from_files(cls, alphabet_file, n, *files):
        """
        Generates an NGram object from a bunch of input files, with the provided alphabet file, n, and files.
        :param alphabet_file: the path to the alphabet file.
        :param n: the length "n" for n-grams.
        :param files: a list of files to parse
        """
        try:  # Verify alphabet file exists and isn't a directory
            with open(alphabet_file, "r") as a_file:
                alphabet = a_file.read().strip()  # alphabet file contains trailing newspaces sometimes
                alphabet = ''.join(sorted(alphabet))  # Helps preserve "alphabetical" ordering. (Lexicographic)
        except FileNotFoundError:
            print("Could not open alphabet file \'{}\'".format(alphabet_file), file=sys.stderr)
            sys.exit(1)
        except IsADirectoryError:
            print("Provided alphabet file \'{}\' is a directory".format(alphabet_file), file=sys.stderr)
            sys.exit(1)

        n_grams = [''.join(permutation) for permutation in [item for item in product(alphabet, repeat=n)]]

        generated_object = NGram(n_grams=n_grams, n=n)

        for f in files:
            generated_object.count_frequencies(f)
            generated_object.calc_entropy()
            generated_object.calc_entropy_stats()
            generated_object.calc_n_gram_stats()

        return generated_object

    def calc_entropy(self):
        """
        Calculate Shannon entropy for each individual file
        """
        for file in self.all_data:
            # set default here with .values() is faster in tests than within the inner for-loop
            self.entropy_values.setdefault(file, 0)
            for n_gram in self.all_data[file].keys():
                if self.all_data[file][n_gram] != 0:
                    self.entropy_values[file] -= self.all_data[file][n_gram] * (log2(self.all_data[file][n_gram]))

    def calc_n_gram_stats(self):
        """
        Calculates averages and standard deviation for each n-gram across all input files
        """
        n_gram_values = {}
        # get a list of n_gram frequencies independent of data file
        for f in self.all_data:
            for n_gram in self.all_data[f]:
                n_gram_values.setdefault(n_gram, [])
                n_gram_values[n_gram].append(self.all_data[f][n_gram])
        for n_gram in n_gram_values:
            self.frequency_averages[n_gram] = mean(n_gram_values[n_gram])
            self.frequency_standard_deviations[n_gram] = pstdev(n_gram_values[n_gram])
            self.frequency_standard_error[n_gram] = self.frequency_standard_deviations[n_gram] / (len(n_gram_values[n_gram]) ** 0.5)

    def calc_entropy_stats(self):
        """
        Calculates average and standard deviation of Shannon entropies calculated in NGram.calc_entropy()
        """
        self.entropy_average = mean(self.entropy_values.values())
        self.entropy_standard_deviation = pstdev(self.entropy_values.values())

    def __prepare_data_file(self, data_file):
        """
        Prepares an individual data file to count frequencies
        :param data_file: path to data file
        :return: a list with all lines
        """
        if path.exists(data_file):
            # Initialize dicts for 'permutations', 'distributions', and 'entropy' for each n-gram
            data = []  # array comprised of each line

            # open data_file in read-mode
            with open(data_file, "r") as d_file:
                for line in d_file:
                    if line.strip():  # append only if the line isn't comprised of only whitespace
                        data.append(line.strip())
            self.all_data[data_file] = {x: 0 for x in self.n_grams}
        else:
            print("Data file \'{}\' does not exist.".format(data_file), file=sys.stderr)
            sys.exit(1)
        return data

    def count_frequencies(self, data_file):
        """
        Counts the frequencies in the data_file parameter
        :param data_file: path for the data_file (used as key in self.all_data)
        :return: None
        """
        data = self.__prepare_data_file(data_file)

        seq_length = 0
        for line in data:
            stripped = line.strip()  # strip white space

            if len(stripped) - self.n + 1 > 0:
                seq_length += len(stripped) - self.n + 1  # calculate how many n-grams are on this line, and if it's 1 or greater, add it to the sequence length

            if seq_length < self.n and seq_length != 0:
                print("\'n\' for n-gram argument is longer at least one line in the data file.", file=sys.stderr)
                continue

            # Note on str.count() function:
            #   str.count() doesn't count overlapping occurrences.
            #   e.g. "AAAB", whilst looking for "AA", will only return 1 occurrence.
            # There ARE regex possibilities for this, but I think this is clear and simple.
            start = 0
            end = self.n
            while end <= len(stripped):
                self.all_data[data_file][stripped[start:end]] += 1
                start += 1
                end += 1

        # we had the number of occurrences, now convert it to frequencies.
        for key in self.all_data[data_file].keys():
            self.all_data[data_file][key] /= seq_length

    def n_gram_to_csv(self, output_dir, file_helper):
        """
        Outputs NGram frequency information into a csv.
        :param output_dir: the output directory
        :param file_helper: the file_helper that holds relevant file names and directories
        """
        with open(path.join(output_dir, "all_{}_frequencies_{}gram.csv".format("_".join(file_helper.directories), self.n)), "w+") as ff:
            ff.write("n-gram,Average Frequency,Frequency Standard Deviation,Frequency Standard Error,")
            ff.write(','.join(self.all_data.keys()))
            ff.write("\n")

            output_dict = {}
            # Get average, standard deviation, standard error, and then append the n_gram's frequency for each file.
            for n_gram, average in self.frequency_averages.items():
                output_dict[n_gram] = [str(average)]
            for n_gram, sd in self.frequency_standard_deviations.items():
                output_dict[n_gram].append(str(sd))
            for n_gram, se in self.frequency_standard_error.items():
                output_dict[n_gram].append(str(se))
            for f in self.all_data:
                for n_gram in self.all_data[f]:
                    output_dict[n_gram].append(str(self.all_data[f][n_gram]))
            for n_gram, frequency in output_dict.items():
                ff.write("{},{}\n".format(n_gram, ','.join(frequency)))

        # for f in self.all_data:
        #     with open(path.join(output_dir, "{}_{}_frequencies_{}gram.csv".format(path.basename(f), "_".join(file_helper.directories), self.n)), "w+") as ff:
        #         ff.write("n_gram,frequency\n")
        #         for n_gram, frequency in self.all_data[f].items():
        #             ff.write("{},{}\n".format(n_gram, str(frequency)))

    def entropies_to_csv(self, output_dir, file_helper):
        """
        Converts NGram's entropy information into a .csv file
        :param output_ddir: the directory to output the csv file into
        :param file_helper: the file_helper object that holds relevant directories/file names.
        """
        with open(path.join(output_dir, "all_{}_entropies_{}gram.csv".format("_".join(file_helper.directories), self.n)), "w+") as entropies_file:
            entropies_file.write("Average Entropy,Entropy Standard Deviation,")
            files = ','.join(self.all_data.keys())
            entropies_file.write(files)
            entropies_file.write("\n")

            entropies_file.write(str(self.entropy_average))
            entropies_file.write(",")
            entropies_file.write(str(self.entropy_standard_deviation))
            entropies_file.write(",")
            entropies_file.write(','.join(map(lambda x: str(x), (self.entropy_values.values()))))


class Chart:
    @staticmethod
    def _help_colors(y):
        # sets the right color
        # For pie charts, helps to ensure the "wrap around" isn't the same color.
        colors_8 = ["#003f5c", "#2f4b7c", "#665191", "#a05195", "#d45087", "#f95d6a", "#ff7c43", "#ffa600"]
        colors_7 = ["#003f5c", "#374c80", "#7a5195", "#bc5090", "#ef5675", "#ff764a", "#ffa600"]
        colors_6 = ["#003f5c", "#444e86", "#955196", "#dd5182", "#ff6e54", "#ffa600"]
        colors_5 = ["#003f5c", "#58508d", "#bc5090", "#ff6361", "#ffa600"]
        colors = colors_8
        for factor in range(5, 9):
            if len(y) % factor != 1:
                colors = eval("colors_{}".format(str(factor)))
        return colors

    @staticmethod
    def _help_single(permutations, ordered=True, reverse=False, se=None):
        """
        Helps to generate the values from the provided permutations
        :param ordered: sort values or not
        :param reverse: whether to reverse the sort
        :param se: standard error
        :return: list <values>, tuple that consists of (<n_grams>, <frequencies>) or (<n_grams, <frequencies>, <se>)
        """

        if ordered:
            # zip together to make an iterable comprised of tuples of (n_gram, frequency, standard error)
            #   e.g. [("ABC", 0.5, 0.05), ("AAA", 0.1, 0.01), ("BBB", 0.4, 0.0005)]
            # We then sort these tuples; we specify the values to compare are the [1] index of the tuples, i.e. the frequency.
            # Once we've got these tuples sorted, we then "unzip" them; so values looks like ("ABC", "BBB, "AAA"), (0.5, 0.4, 0.1), (0.05, 0.0005, 0.01)
            if se is not None:  # we don't have SE for individual files, so let's account for that
                values = (zip(*sorted(zip(permutations.keys(), permutations.values(), se.values()),
                                      key=lambda t: t[1], reverse=reverse)))
            else:
                values = (zip(*sorted(zip(permutations.keys(), permutations.values()),
                                      key=lambda t: t[1], reverse=reverse)))
        else:
            if se is not None:
                values = (list(permutations.keys()), list(permutations.values()), se.values())
            else:
                values = (list(permutations.keys()), list(permutations.values()))

        return values

    @staticmethod
    def dendrogram_generator(frequencies, output_folder, directories):
        """
        Used to generate Manhattan and Jensen-Shannon distance dendrograms
        :param frequencies: n_gram data, takes the form of NGram.all_data (i.e. a dictionary of n_gram data)
        :param output_folder: folder to save figure in
        :param chart_args: **kwarg type argument, of form ParseHelper.charts
        """
        # Get title/save file name
        title = ", ".join(directories)
        file_name = "_".join(directories)

        # Get n
        n = len(list(frequencies[list(frequencies.keys())[0]].keys())[0])

        # Generate np.array for values
        frequencies_for_dist = {}
        for name in frequencies.keys():
            frequencies_for_dist[name] = list(frequencies[name].values())
        X = np.array(list(frequencies_for_dist.values()))

        # MANHATTAN DISTANCE
        # For future developers (?) check Lab Book for more details
        # Essentially, give it the values, it will calculate the distance itself, and then create the dendrogram
        fig = figure_factory.create_dendrogram(X, labels=list(frequencies_for_dist.keys()), distfun=lambda x: pdist(x, metric='cityblock'))
        fig.update_layout(height=3000, width=len(frequencies_for_dist.keys()) * 16, title_text=title)  # Set size
        # the cdn one is smaller
        fig.write_html(path.join(output_folder, "{}_{}gram_dendrogram_manhattan.html".format(file_name.strip('/'), n)))
        fig.write_html(path.join(output_folder, "{}_{}gram_dendrogram_manhattan_cdn.html".format(file_name.strip('/'), n)), include_plotlyjs='cdn')

        # JENSEN SHANNON
        # Same for above
        fig = figure_factory.create_dendrogram(X, labels=list(frequencies_for_dist.keys()), distfun=lambda x: pdist(x, metric='jensenshannon'))
        fig.update_layout(height=3000, width=len(frequencies_for_dist.keys()) * 16, title_text=title)
        fig.write_html(path.join(output_folder, "{}_{}gram_dendrogram_jensenshannon.html".format(file_name.strip('/'), n)))
        fig.write_html(path.join(output_folder, "{}_{}gram_dendrogram_jensenshannon_cdn.html".format(file_name.strip('/'), n)), include_plotlyjs='cdn')

    @staticmethod
    def single_pie(n_gram_data, file_name, directory, output_folder, **chart_args):
        """
        Used to generate a pie chart
        :param n_gram_data: n_gram data, takes the form of NGram.all_data[file] (i.e. a dictionary)
        :param file_name: file_name, used only for titles/save file name
        :param directory: directory of file_name, used only for titles/save file name
        :param output_folder: folder to save figure in
        :param chart_args: **kwarg type argument, of form ParseHelper.charts
        """
        # Initialize data values
        try:
            x, y = Chart._help_single(n_gram_data, chart_args["order"], chart_args["reverse"])  # get our x/y values. No point in getting standard error, so don't care.
        except ValueError as error:
            print("Chart.single_pie: Could not properly generate display values.\n"
                  "Error message: \"{}\"".format(str(error)), file=sys.stderr)
            sys.exit(1)
        n = len(x[0])

        # Filter 0s
        x = [val for idx, val in enumerate(x) if y[idx] != 0]
        y = [val for val in y if val != 0]

        # Warn if data set is too large
        if len(y) > 8:
            print("{} values to be displayed on pie-chart. May be difficult to read.".format(len(y)))

        # Set graph styles
        plt.style.use('ggplot')
        colors = Chart._help_colors(y)

        # Set graph values
        plt.pie(y, labels=x, colors=colors, startangle=90, rotatelabels=True, labeldistance=1.05, wedgeprops={"edgecolor": "white", "linewidth": 0.2}, normalize=True)

        # Set graph title
        plt.title("{0}-gram frequencies for {1}\nn={0}, from {2}".format(n, file_name, ', '.join(directory)))

        # Save figure
        if not isinstance(directory, list):
            directory = [directory, ]
        ordered = "sorted" if chart_args["order"] else "lexicographic"
        plt.savefig(FileHelper.generate_file_name("{file_name}_{directory}_{sorted}_piechart_{n}gram.pdf",
                    output_folder, {"n": n, "file_name": path.split(file_name)[-1], "sorted": ordered, "directory": "_".join(directory)}),
                    bbox_inches='tight')  # use bbox_inches='tight' to erase all the whitespace on all the sides
        plt.close()

    @staticmethod
    def single_bar(n_gram_data, file_name, directory, output_folder, **chart_args):
        """
        Used to generate a bar chart
        :param n_gram_data: n_gram data, takes the form of NGram.all_data[file] (i.e. a dictionary)
        :param file_name: file_name, used only for titles/save file name
        :param directory: directory of file_name, used only for titles/save file name
        :param output_folder: folder to save figure in
        :param chart_args: **kwarg type argument, of form ParseHelper.charts
        """
        # Initialize data values
        error = None
        try:
            if "se" not in chart_args:
                x, y = Chart._help_single(n_gram_data, chart_args["order"],
                        chart_args.get("reverse",False))
            else:
                x, y, error = Chart._help_single(n_gram_data, chart_args["order"],
                        chart_args.get("reverse",False), chart_args["se"])
        except ValueError as error:
            print("NGram.single_bar: Could not properly generate display values.\n"
                  "Error message: \"{}\"".format(str(error)), file=sys.stderr)
            sys.exit(1)
        if error is not None:
            error = [1.96 * se for se in error]
        n = len(x[0])

        # Filter zeros
        if chart_args["filter"]:
            x = [val for val in x.values() if val != 0]
            y = [key for key in y if key != 0]

        # Set graph styles
        plt.style.use('ggplot')
        colors = Chart._help_colors(y)
        plt.xlim(-0.5, len(x) - 0.5)  # Try to keep the x-axis length proportionate with how many values, instead of matplotlib autoadjusting the axis width
        plt.xticks(range(0, len(x)), x, fontsize=8)  # along each x-tick, label with x
        plt.margins(x=0)
        plt.setp(plt.gca().get_xticklabels(), rotation=90)  # Rotate the x-tick labels so we can actually see them and fit more
        plt.gcf().set_size_inches(len(x) * 0.3 + 4, max(y) * 20 + 8)

        # Set graph values
        if error is not None:
            capsize = 20 / (len(x)**0.25)  # Try to adjust capsize to fit the b ar widths
            plt.bar(range(0, len(x)), y, yerr=error, capsize=capsize, color=colors)
        else:
            plt.bar(range(0, len(x)), y, color=colors)

        # Graph titles
        plt.xlabel("N-Grams")
        plt.ylabel("Frequencies")
        if not isinstance(directory, list):
            directory = [directory, ]
        plt.title("Frequency vs. {0}-gram for {1}\nn={0}, from {2}".format(n, file_name, ', '.join(directory)))

        # Save figure
        ordered = "sorted" if chart_args["order"] else "lexicographic"
        plt.savefig(FileHelper.generate_file_name("{file_name}_{directory}_{sorted}_barchart_{n}gram.pdf",
                    output_folder, {"n": n, "file_name": path.split(file_name)[-1], "sorted": ordered, "directory": "_".join(directory)}),
                    bbox_inches='tight')  # use bbox_inches='tight' to erase all the whitespace on all the sides
        plt.close()  # FROM WHAT I'VE OBSERVED, have to close the plt if I want to generate another.

    @staticmethod
    def _help_compare(order, r, *n_gram_objects: NGram):
        """
        Compares and formats the x/y/y-error for comparisons
        :param order: whether or not to sort the data (True/False)
        :param r: whether or not to reverse the sort
        :param n_gram_objects: the NGram objects to compare
        """

        """
        This stuff was when I thought Professor Nehaniv was trying to compare individual test subjects to each other.
        """
        # # Generate file pairings
        # first_files = list(n_gram_objects[0].all_data.keys())
        # skip_first = iter(n_gram_objects)
        # next(skip_first)

        # first_file_names = {path.basename(key): key for key in n_gram_objects[0].all_data.keys()}
        # pairings = [{} for _ in first_file_names]

        # for n_gram_object in skip_first:
        #     max_tolerance = 1-1/min(set(len(path.basename(k)) for k in n_gram_object.all_data.keys()))
        #     loop_file_names = {path.basename(key): key for key in n_gram_object.all_data.keys()}

        #     for i, f in enumerate(first_file_names):
        #         try:
        #             match = get_close_matches(f, loop_file_names.keys(), 1, max_tolerance)[0]
        #         except IndexError:
        #             print("{} did not get a good match while looking for comparative files.".format(path.basename(f)), file=sys.stderr)
        #             continue
        #         long_name = loop_file_names[match]
        #         del loop_file_names[match]
        #         pairings[i][long_name] = n_gram_object.all_data[long_name]

        #         if first_file_names[f] not in pairings[i]:
        #             pairings[i][first_file_names[f]] = n_gram_objects[0].all_data[first_file_names[f]]

        # Deal with that another day, let's get averages going

        # For aggregate-to-aggregate comparisons
        if order:
            (keys, values, errors) = (zip(*sorted(zip(n_gram_objects[0].frequency_averages.keys(),
                                                      n_gram_objects[0].frequency_averages.values(),
                                                      n_gram_objects[0].frequency_standard_error.values()),
                                                  key=lambda t: t[1],
                                                  reverse=r)))
        else:
            (keys, values, errors) = (n_gram_objects[0].frequency_averages.keys(),
                                      n_gram_objects[0].frequency_averages.values(),
                                      n_gram_objects[0].frequency_standard_error.values())

        average_n_grams = tuple(keys)
        average_frequencies = []
        average_se = []
        for others in n_gram_objects:
            average_frequencies.append(tuple(others.frequency_averages[key] for key in keys))
            average_se.append(tuple(others.frequency_standard_error[key] for key in keys))
        return average_n_grams, average_frequencies, average_se

    # @staticmethod
    # def compare_bar(output_folder, *all_datas: NGram):
    #     """
    #     Essentially a layer for multi_bar. Takes all the directory names and makes sure they're all in the chart titles.
    #     :param output_folder: the output folder to place the generated PDF charts.
    #     :param all_datas: the NGram objects to compare.
    #     """

    #     """
    #     This stuff was when I thought Professor Nehaniv was trying to compare individual test subjects to each other.
    #     """
    #     # file_count = len(all_datas[0].all_data.keys())

    #     # # Copied from https://stackoverflow.com/questions/10825051/checking-to-see-if-a-list-of-lists-has-equal-sized-lists
    #     # if not all(len(arg.all_data.keys()) == file_count for arg in all_datas):
    #     #     print("One of the provided NGram objects has more files than another. Generation for comparative bar graphs may be faulty.", file=sys.stderr)

    #     files = []
    #     for i, arg in enumerate(all_datas):
    #         files.append([])

    #         for f in arg.all_data:
    #             # Set directories
    #             if path.dirname(f) not in files[i]:
    #                 files[i].append(path.dirname(f))

    #     a_n_grams, a_frequencies, a_se = Chart._help_compare(True, True, *all_datas)
    #     Chart.multi_bar(files, a_n_grams, a_frequencies, a_se, output_folder)

    # @staticmethod
    # def multi_bar(file_names, n_gram_keys, frequencies, errors, output_folder):
    #     """
    #     Generates a spaced-out comparison bar chart, \"well-defined\" for up to 5-sets of data
    #     """
    #     # Colours for the chart's data
    #     all_colors = {2: ["#003f5c", "#ffa600"],
    #                   3: ["#003f5c", "#bc5090", "#ffa600"],
    #                   4: ["#003f5c", "#7a5195", "#ef5675", "#ffa600"],
    #                   5: ["#003f5c", "#58508d", "#bc5090", "#ff6361", "#ffa600"]}

    #     # Variables to help adjust graph size/styles
    #     num_grams = len(n_gram_keys)
    #     num_files = len(file_names)
    #     n = len(n_gram_keys[0])

    #     # Set graph styles
    #     capsize = 20 / ((num_grams * num_files)**0.25)
    #     plt.style.use('ggplot')
    #     try:
    #         colors = all_colors[num_files]
    #     except KeyError:
    #         print("Number of files for comparison bar chart exceeds 5, using default colors as fallback.", file=sys.stderr)
    #         colors = [None] * num_files

    #     # Set x-ticks
    #     plt.xlim(-0.5 * num_files, (num_grams * num_files) - (num_files * 0.5))  # Try to keep the x-axis length proportionate with how many values, instead of matplotlib autoadjusting the axis width
    #     plt.xticks(np.arange(0, num_files * num_grams, step=num_files), n_gram_keys, fontsize=8)  # along each x-tick, label with x
    #     # Explanation on xticks in lab_book.md
    #     plt.margins(x=0)
    #     plt.setp(plt.gca().get_xticklabels(), rotation=90)  # Rotate the x-tick labels so we can actually see them and fit more
    #     plt.gcf().set_size_inches(num_grams * num_files * 0.3 + 4, max(max(frequencies)) * 20 + 8)

    #     # Set graph values
    #     x_intervals = []
    #     if errors is None:
    #         errors = [None] * num_files
    #     for offset in np.linspace(-(num_files - 1) * 0.4, (num_files - 1) * 0.4, num_files):
    #         x_intervals.append([num_files * i + offset for i in range(num_grams)])
    #     for i, frequency in enumerate(frequencies):
    #         # Have to do y_values to ensure the 'order' of n_grams are correct
    #         plt.bar(x_intervals[i], frequency, color=colors[i], label=', '.join(file_names[i]), capsize=capsize, yerr=errors[i])
    #     plt.legend()

    #     # Graph titles
    #     plt.xlabel("N-Grams")
    #     plt.ylabel("Frequencies")
    #     plt.title("Frequency vs. n-gram comparison, n={0}\n{1}".format(n, ' vs. '.join([', '.join(name) for name in file_names])))  # TODO: fix here

    #     plt.savefig(FileHelper.generate_file_name("{files}_comparison_barchart_{n}gram.pdf", output_folder,
    #                 {"files": '_vs_'.join([('_').join(name) for name in file_names]), "n": n}), bbox_inches='tight')
    #     plt.close()



class FileLengthError(Exception):
    pass


class PathLengthError(Exception):
    pass


class FileHelper:
    # Utility class just to help with file names, directories, etc
    def __init__(self, directories=[]):
        if not isinstance(directories, list):
            directories = [directories]
        self.directories = directories
        self.__rel_files = []  # Protect these variables; rel_path is relative path to the file
        self.__abs_files = []  # Absolute path to the file
        self.__files = []  # Only file name (no directories)

    @staticmethod
    def check_string_length(file_name):
        # Ensure these file names and paths aren't too long.
        if len(path.basename(file_name)) > pathconf('.', 'PC_NAME_MAX'):
            raise FileLengthError("File name length is longer than maximum file name length {}".format(pathconf('.', 'PC_NAME_MAX')))
        elif len(file_name) > pathconf('.', 'PC_PATH_MAX'):
            raise PathLengthError("Path length is longer than maximum path lenght {}".format(pathconf('.', 'PC_PATH_MAX')))
        else:
            return file_name

    @staticmethod
    def generate_file_name(format_string, output_folder, format_dict):
        file_name = path.join(output_folder, format_string.format(**format_dict))
        return FileHelper.check_string_length(file_name)

    @property
    def rel_files(self):
        return self.__rel_files

    @property
    def abs_files(self):
        return self.__abs_files

    @property
    def files(self):
        return self.__files

    @property
    def file_name(self):
        return self.__file_name

    @file_name.setter
    def file_name(self, *args, format_string="{}", split="_", parent_dir=[]):
        """
        :param args: allows as many parent directories as desired.
        :param format_string: a string, unformatted (i.e. with {}) to format
        :param split: the character to split the directories
        :param parent_dir: the directories "above" the file, to help save in the right place
        """
        temp_file_name = path.join(*parent_dir, format_string.format(split.join(self.directories)))
        self.check_string_length(temp_file_name)
        self.__file_name = temp_file_name

    def append_directory(self, directory):
        # Shouldn't need a directory deleter
        self.directories.append(directory)

    def append_file(self, f):
        self.__rel_files.append(path.relpath(f))
        self.__abs_files.append(path.abspath(f))
        self.__files.append(path.basename(f))
