(Note the following was performed on a Macbook)
# Steps to getting started

1. Create a Compute Canada account. (This includes getting the appropriate CRI and information from Professor Nehaniv)
    * Might be useful; I recall UWaterloo is under "Sharcnet"
2. ssh into Compute Canada. For me, my user ID is acchen, so I've used `ssh acchen@graham.computecanada.ca`; it may ask you to verify some keys, I just chose yes.
    * Note: I've chosen Graham instead of Cedar just because Graham is hosted at the U of Waterloo, and purely also by chance.

## Python specific:

Note that there should already by a virtual environment under WAICL-CSSR. In that case, you can ignore everything in "Python specific." To activate the virtual envirornment, just do "source env/bin/activate" in WAICL-CSSR. For more info, check README.md

1. If you're using Python, I suggest you run `module avail Python` to see what versions of Python are available.
2. Load your desired version of Python; `module load python/3.x`. See https://docs.computecanada.ca/wiki/Python for more info.
3. Navigate to your desired project. It should be under `~/projects/` somewhere; in this case, I found it under `~/projects/def-nehaniv`.
4. Initialize a virtual environment. I've elected to use the suggested way as per the docs on the Compute Canada website, but there are multiple ways of creating virtual environments.
	* `virtualenv --no-download path/to/env`
	* Activate the environment: `source path/to/env`
	* **Upgrade pip (or see if it's the right version.)** `pip install --no-index --upgrade pip`
	* Install any packages you might need. In my case, `pip install matplotlib`. Note that Compute Canada has a nice module, SciPy, that takes care of many packages. This can be activated usinig `module load scipy-stack` _outside_ the virtual environment. (It may work inside too, but I haven't tried.)
	* To exit the virtual environment, `deactivate`.

## Note:
Do not copy to the individual user folders underneath the project if it's meant to be shared. From what I can observe, the individual users' folders are inaccessible to other people.

## Transferring files:
https://docs.computecanada.ca/wiki/Transferring\_data may be incredibly useful. As a "quick start", on Mac:
`sftp userid@graham.computecanada.ca`

To transfer file from local to Compute Canada once in sftp:
`put <file name>`
For directories, two commands are needed donce in sftp:
`mkdir <directory name as it is locally>`
`sftp -r <directory name>`

To transfer file from remote to local once in sftp:
`get <file name>`
Directories once in sftp:
`get -r <directory name>`
