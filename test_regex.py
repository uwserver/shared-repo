from run_n_gram import glob_file_regex

import sys

print("Regex Received:", end=" ")
print(sys.argv[1:])
print("File Selection:")

reg = []
for r in sys.argv[1:]:
    reg.extend(glob_file_regex(r) )
for f in sorted(reg):
    print(f)
