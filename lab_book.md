This is only for miscellaneous notes/reference. May be poorly formatted. ctrl+F is your good friend.

- [Potential stuff to fix/improvements/potential bugs](#potential-stuff-to-fiximprovementspotential-bugs)
  - [Help message/argparse help messages](#help-messageargparse-help-messages)
  - [Pie chart colour wheel](#pie-chart-colour-wheel)
  - [Individuals' comparison](#individuals-comparison)
  - [Structure](#structure)
- [Notes](#notes)
  - [xticks in multi_bar](#xticks-in-multi_bar)
  - [set_size_inches/chart size in general](#set_size_incheschart-size-in-general)
  - [offset and other spacing in multi_bar](#offset-and-other-spacing-in-multi_bar)
  - [Colors for charts](#colors-for-charts)
- [Sort used in charts](#sort-used-in-charts)
  - [Dendrogram **distances**](#dendrogram-distances)
  - [Running on ComputeCanada](#running-on-computecanada)

# Potential stuff to fix/improvements/potential bugs

## Help message/argparse help messages

Prior to using argparse, I wrote the help message myself, and was intent on parsing the sys.argv myself. It was not until after I discovered argparse that I realized the help message was not the standard way to go about it. This might need some improvement in the future.

## Pie chart colour wheel

Pie charts aren't used very often, but should we need pie-charts, we want to ensure that the colour doesn't "loop back" and cause one mega-region. I _believe_ the loop I have right now eliminates this, but I have not tested this.

## Individuals' comparison

Should something similar ever be needed again, a few notes. In help_compare, I have a bunch of commented out code. 

One of the challenges is how to match the files properly together. If we have a bunch of input files and we want to match individual files, how do we determine _which_ files should be compared?

The solution: get_close_matches from difflib. Essentially, we want to match file names that are _close together_, for example C_tests_subject_1 and M_test_subject_1 should be matched together. Max-tolerance is the key here; from what I've observed, it seems like the string's similarity, computed via \[identical characters\]/\[total original characters\] >= "max tolerance". Therefore, we want the _shortest_ length strings to be able to matchup, because the bound will be fulfilled for larger length strings too; if the tolerance is too low, we'll get all sorts of matches. (Just as a note; tolerance of 0 means that all the possible matches, i.e. the second argument, will be returned.)

## Structure

Right now, the structure is a little weird; I wasn't too sure what the end-result was gonna look like, so the more I went, the more it changed. It went from an "all-in-one-n_gram.py" to adding some CLI help messages, to running that from a subprocess.

# Notes

## xticks in multi_bar

`np.arange` helps to "take steps" through a range. The number of x_values we have is num_files\*num_grams. (We have num_files, with num_grams in each file). Thus, we want to evenly space num_files\*num_grams. 

For spacing, we can think of it this way. We only want the n_gram label for each "group"; how wide is the distance between each label? The number of files. Alternatively, we know our x-width is num_files\*num_grams, and we want to fit num_grams number of labels in that width. num_files\*num_grams/num_grams = num_files, so that's our step distance.

## set_size_inches/chart size in general

**All the values here are magic numbers.** Need to size-chart varying on amount of data, as matplotlib was "crowding" the x-tick labels.

## offset and other spacing in multi_bar

plt.bar can take in a "list" for positions. Using the range(n) argument, we only get evenly spaced positions. We need to accomodate num_files\*num_grams x-values.

The bar width is by default 0.8, but the "spacing" means it is technically 1; you have 0.1 spacing on each side of the bar, and 0.8 for the bar width.

For clarity's sake, I wanted all the bars for one n-gram smushed together. This means that some bars need to get moved over by multiples of 0.4 units. (We don't care about the 0.1 "spacing" because we want to _eliminate_ the spacing.) 

I use np.linspace to generate an even distribution of offsets. -(num_files-1)\*0.4 and (num_files-1)\*0.4 will perfectly generate the offsets. Afterwards, we generate _multiples_ of the output of range(), andd then tack on the offset. 

## Colors for charts

THIS SHOULD NO LONGER BE AN ISSUE. Turns out gg-plot has their own colours which don't look too bad. originally, the colours looked poor and so I used [this colour palette](https://learnui.design/tools/data-color-picker.html)

# Sort used in charts

Problem: dictionary isn't ordered, lists can't map a string to a value.

If you're already familiar with how the lambda function works, this explanation is useless. But basically, we do a few things here. First, we zip together keys(), values(), and sometimes errors(). This is because we might sort the keys or the values, but we still want to maintain the mapping of key: value. Thus, we zip them together. In each "zipping", we have \[key, value, error\], and we want to sort by value. The lambda function takes the zipping, and it sorts according to the output. Since we want to sort by value, we ask it to take the thing in zipping\[1\], or the value. 

Now, we have a zipping, and we sorted all the zippings. Now, to shove it into individual values, for x, y, and error, we want to _unzip_ everythingi again. We use the zip(*) operator here. 

## Dendrogram **distances**

A few takeaways here. While usually we might have a symmetrical distance matrix, **we do not need it for the figure_factory.create_dendrogram**. Instead, we supply it with a distance function. You can find all the possible distances https://docs.scipy.org/doc/scipy/reference/generated/scipy.spatial.distance.pdist.html here. *Because of the difficulty of implementing our own distance functions/the fact most already exist*, I have specifically not tested this, but it seems like this is how the "distance" return is formatted:

Let d_{a, b} be the distance between the values at index a and b. 

The returned distance should look like this:
\[d_{0, 1}, d_{0, 2}, d_{0, 3}...d_{0, n}, d_{1, 2}, d{1, 3}...d_{1, n}, d_{2, 3}, ..., d_{2, n}... d_{n-1, n}\]

## Running on ComputeCanada

**NOHUP IS YOUR BEST FRIEND**
nohup python all.py & will run all.py in the background, will keep running after you end the ssh session, and will output into nohup.out; it is the primary way _I_ have run all.py