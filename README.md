This document is meant to help any future developers debug code/extend features.
***
- [Quickstart](#quickstart)
  - [Entire "suite" (multiple directories) of files](#entire-suite-multiple-directories-of-files)
  - [Only a single folder (i.e. just `run_n_gram.py`)](#only-a-single-folder-ie-just-run_n_grampy)
- [Development information](#development-information)
  - [Python environment](#python-environment)
    - [Packages](#packages)
  - [Basic top-down view](#basic-top-down-view)
  - [Files](#files)
    - [`n_gram.py`](#n_grampy)
    - [`run_n_gram.py`](#run_n_grampy)
  - [Miscellaneous](#miscellaneous)
    - [Virtual Environments](#virtual-environments)
    - ["cdn" dendrograms, `Orca`, and `kaleido`, and ComputeCanada](#cdn-dendrograms-orca-and-kaleido-and-computecanada)
***
# Quickstart

## Entire "suite" (multiple directories) of files

1. Move your `alphabet.txt` and the `sequences` directory (these names can be changed, see below) into the same directory as the Python files
2. Go into `all.py`, and modify your `max_n`; this will perform the Python n-gram operations on n-grams from length 1 to `max_n`
3. Modify your output directory in `all.py`, with the variable `output_directory`. _This is a relative path_
4. Modify your iniput directories in `all.py`, with the variable `i_dir`
   * Put the directories you want "compared" in the same tuple. For example, if I have `[(a, b), (c, d)]`, the n-gram operations will be performed on the files in directories a, b, c, and d, but then a and b will be compared, as well as c and d. 
5. Your output will be generated in `./output`

## Only a single folder (i.e. just `run_n_gram.py`)

The general syntax should have explanations in `python run_n_gram.py help`, but here is another explanation/a few examples:

Syntax: `python run_n_gram.py <alphabet-file (REQUIRED)> <n (REQUIRED)> <--files OR --dir> (relative path to files/directories for input) [--output <output directory>] [--regex <regex pattern>] [--chart (SEE BELOW)]`

* The alphabet-file and n arguments are positional and requiried. They must be the first and second argument respectively, and must exist.
* --files OR --dir must exist. Note that --dir will group the files from all arguments in --dir together; that is, if I use `--dir a/b ../c`, the input files will be all the files in `a/b`, `../c`, and they will be treated as a _single_ group of input files. 
* --output is optional, and specifies a relative path for an output directory. If the directory doesn't exist, it will create the directory. Note that currently, if the output directory is non-empty, the script will warn the user and stop.
* --regex can be used to supple a regex pattern that the files should match. See [Python's re library](https://docs.python.org/3/library/re.html) for information about the regex "flavor" used.
* --chart is used to generate bar charts, pie charts, or both. It has a few optional arguments.
    1. --fz=True/False, to remove all the "0" values from the data. Default should be false for bar charts, and _always_ true (unchangeable) for pie charts.
    2. -o=True/False, to sort the data.
        * -r=True/False, to "reverse" the sort. **The default (i.e. -r=false) is ascending, which means that the largest will be on the right.**
    3. -f, to specify which files should have charts generated. *By default it will generate the specified chart(s) for _all_ input files.
   
  Example usage: `--chart pie bar -fz=False -o=True -r=Reverse` will generate pie and bar charts for all input files, with 0 values included in the bar chart, sorted descending (largest on the left).

# Development information

Everything here should be correct and """official""". Most, if not all functions should have help strings that conform to the Python standard. 

## Python environment

I've aligned, to the best of my ability, to PEP8, with the notable exception of ignoring line-length. However, variables use snake_case and class names use PascalCase aka UpperCamelCase.

macOS version 10.15.5 (should not matter)

Python 3.7.6, **installed using Homebrew**.

### Packages 

_Also in [requirements.txt](requirements.txt)._ This is generate with `pip freeze`; **in reality, this should be strictly limited to scipy, plotly, and matplotlib. numpy is needed as well, but should be included in scipy.**

```certifi==2020.6.20
cycler==0.10.0
kiwisolver==1.2.0
matplotlib==3.3.1
numpy==1.19.1
Pillow==7.2.0
plotly==4.9.0
pyparsing==2.4.7
python-dateutil==2.8.1
retrying==1.3.3
scipy==1.5.2
six==1.15.0
```

To install these packages, try the following:

* Easiest way, with a 'perfect' environment: `pip install -r requirements.txt`. Watch out for `pip` thinking it's Python 2.
* If you're worried about pip not differentiating between python2 or python3: `python3 -m pip install -r requirements.txt`
* If you suspect the packages specified in `requirements.txt` is suspect, try individually doing `pip install scipy`, `pip install matplotlib`, and `pip install plotly`.
* If you're getting permission errors, because python is at your root or other issues, try adding the `--user` flag to the end of your command, or try running the commands in a [virtual environment](#virtual-environments).

## Basic top-down view

The program currently works as follows, to operate with the intended goals of generating output. `n_gram.py` has the basic skeleton functions and classes. 

`run_n_gram.py` actually uses the objects and classes declared in the aforementioned files, and runs through the program depending on its arguments; it generates .csv files, chart .pdf files, and dendrogram.html files. 

`all.py` is used to 'automate' some of the work of `run_n_gram.py`. Instead of typing out the entire command to run `run_n_gram.py` for _each_ n (as in n-gram), it uses subprocess to loop through the desired values of n. *In most cases*, running and modifying `all.py` should be suitable and take care of most tasks.


## Files

### `n_gram.py`

This file almost exclusively has actual computation here, meaning that most of the run-time is probably happening with the code in this file. This file is very similar to a module; nothing happens if this file is run by itself.

* `NGram`

    This class, as its name suggests, deals exclusively with n-grams. 
    
    `__init__` takes in pre-generated arguments and sets the member data accordingly.

    `n_gram_from_csv` parses a frequencies.csv file and then returns an NGram object.

    `n_gram_from_files` takes in a bunch of files and parses them, then returns an NGram object.

    `__prepare_data_file` is a **private** helper function. It takes the name of a file name, checks it exists, and more importantly, reads each line and stores it in a list for use in `count_frequencies`.

    `calc_entropy` calculates the Shannon-Entropy, assuming that the frequencies in each file have already been counted.

    `count_frequencies` takes in a file name, and then counts the frequency of each n-gram in the file.

    `calc_n_gram_stats` is used to calculate the _average_, _standard\_deviation_, and _standard\_error_ for the n-gram frequencies.

    `calc_entropy_stats` is used to calculate the _average_ and _standard\_deviation_ for specifically the Shannon entropies.

* `Chart`
    
    This class is exclusively used for chart generation, but there is some "calculation" being done here. Note that there is no "`__init__`" function, because this is used more as a utility class than objects. _There is some work to be done with testing \_help\_colors and bar-graph comparisons, as well as truncating frequencies_.

    `_help_colors` is a helper function that serves only one purpose, which is to ensure that the colors in the pie-chart do not "loop-back" and create a singular large and confusing wedge. 

    `_help_display` is a function that zips/unzips (and sorts) the permutations. It essentially turns a dictionary into a tuple of file names, values, and standard error (if included).

    `dendrogram_generator` generates dendrograms. It generates four dendrograms: one "cdn" dendrogram and one normal dendrogram for both the Manhattan distance and the Jensen-Shannon distances. For info about cdn/normal files, check ["cdn" dendrograms](#cdn-dendrograms-orca-and-kaleido-and-computecanada).

    `display_pie` and `display_bar` generates pie and bar graphs respectively, with the colors from `_help_colors`. **There is a limit on the size of bar graphs**, which means if `n` gets too big, the bar graphs might not generate.

* `FileHelper`
  
    This class is just to hold a bunch of paths and generate paths/titles safely.

    `__init__` sets various member variables to null, and initializes the file input directories.

    `check_string_length` ensures that file names are not too long and similarly, paths are not too long. Various operating systems have different constraints on path and file name length.

    `generate_file_name` safely generates a file name (i.e. checks the name with `check_string_length`) from an unformatted string and a format string. 

    `append_directory` appends directories to the `self.directory` variable.

    `append_file` appends files, adjusting the `abs_path`, `files`, and `rel_files` properties as well.

    Properties: `abs_path`, `files`, `rel_files` are absolute paths, _only_ file names, and relative paths.

### `run_n_gram.py`

Is used as a "mask" for `n_gram.py`; it isn't very robust, but does some basic stuff. In conjunctioin with `all.py`, it accomplished most things needed.

* `ParseHelper`
    
    This class collects command line arguments.

    `__init__` checks whether or not the input requires the `_help` function for output instead of executing further instructions. It also initializes the Parser object and sets variables.

    `_help` contains help strings, and is used in lieu of the `ArgParse`'s implentation of help strings.

    `_init_parser` adds the necessary arguments to the `ArgumentParser` object.

    `set_alphabet_file`, `set_data_files`, `set_n`, `set_output_dir`, and `set_chart_args` check the alphabet file input, data file input, n, output directory, and chart arguments respectively. Each returns a filtered and, especially in the case of `set_chart_args`, "processed" version of the arguments.

## Miscellaneous

### Virtual Environments

Virtual environments offer a few benefits, and their usefulness, I believe, warrants a section here. Note that this is all done on a Mac/Linux (ComputeCanada); Windows may differ.

To _get_ a virtual environment, **please use Python 3.6+**. Once you have a compatible version, run `python3 -m venv <env name>`. This will create a virtual environment, contained in a folder supplied through `<env name>`. 

To activate the environment, run the command `source <env name>/bin/activate`. You should see your terminal turn from something like `user@computer [current directory]` to `(<env name>) user@computer [currrent directory]`. To deactivate, simply type "deactivate" and the terminal should revert.

Once you're here, you can do a few things. One is that if your computer uses python2 by default, or python 3.5.x (as an example), **the python version that you use to create the virtual environment will be the "default" python version**. For example, my Mac uses python2 by default. However, since I used `python3 -m venv env`, when I am in my virtual environment, my python version by default is python 3.7.6 (the default python3 version on my computer). 

There are also a few benefits for pip. One is that all pip installs will be "contained" and isolated here. Packages that might interfere from your computer's "native" packages are inaccessibly here, and similarly, packages you install here are inaccessibly outside the environment. When trying to get a dependency list for packages, pip freeze will only list the packages _in the virtual environment._ Another benefit is that sometimes, using pip without a virtual environment will try to install packages to your computer's root libraries, and the permissions are insufficient. One workaround is to append "--user", but this may be messy or undesirable. Using a virtual environment "bypasses" this, as the packages will be installed onto the virtual environment, thus avoiding the permission issues.

### "cdn" dendrograms, `Orca`, and `kaleido`, and ComputeCanada

plotly isn't quite as """robust""" as matplotlib. One of the way this manifests is the difficulty to generate plain PDFs for the dendrograms. Right now, as of August 31st 2020, it saves dendrograms as .html files. For some html files, you may notice that "cdn" is appended.

plotly's default html generation includes some Javascfipt for the interactivity of the chart. This is not always neededd, but is included anyway. The "cdn" option allows the file to be smaller, by "pointing" to the Javascript code online instead of packaging _with_ the html file. **This does mean that anyone using the cdn files must have an internet connection.**

One of the ways to generate PDF files is using Orca, which is plotly's "retired" Orca. Orca, from my observations, is like an executable. It is _somewhat_ large, with some 20-30 MBs. This allows for PDF generation. However, there is no way to easily `pip` install this, so I have specifically decided to not use Orca to generate PDFs.

What _can_ be installed using `pip` is `kaleido`. The reason I have not included _this_ is because **as of August 20th, it cannot be installed on Compute Canada**. I have tried the following:

* Specifically not using Compute Canada's "wheels" 
* Downloading the wheels directly off of [PyPi](https://pypi.org/project/kaleido/#files)
  * Adding onto this one, changing the "flags" (none-manylinux*) to forcefully make it compatible with the Python/OS on ComputeCanada
* Using the different pip options, e.g. --no-binary

It should be noted that Kaleido works fine on my personal computer (macOS). The problem thusly lies somewhere between Kaleido and CentOS (ComputeCanada), or the "wheel"/Python that is on ComputeCanada.