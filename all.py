import subprocess
import os
import n_gram
from os import path

max_n = 4
output_directory = "output"
i_dirs = [("matchedControls_eventSequences", "Meditators_eventSequences"), ("matchedControls_peakSequences", "Meditators_peakSequences")]

os.mkdir(output_directory)
for directory in i_dirs:
    for d in directory:
        for n in range(1, max_n + 1):
            subprocess.run("python3 run_n_gram.py alphabet.txt {1} --dir {0} --output {2}/{0}_{1} --chart bar -o=True -r=True"
                           .format(d, n, output_directory), shell=True)

mydict = {}
for n in range(1, max_n + 1):
    for pair in i_dirs:
        o = []
        for f in pair:
            o.append(n_gram.NGram.n_gram_from_csv(path.join(output_directory, "{}_{}".format(f, str(n)), "all_{}_frequencies_{}gram.csv".format(f, str(n)))))
    n_gram.Chart.compare_bar(output_directory, *o)
