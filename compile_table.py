# e.g. python compile_table.py 4 "**/C01_R2_*_clock" ["other-regex" ...
# This script compiles a single csv for all data matched by the regex
# Assume individual file basename is distinct.

import glob
import sys
import os
import pandas as pd
import matplotlib.pyplot as plt
import math
import itertools

# Configuration Here
interactive=False # disable confirmation popup
debug_mode=False
alphabet = "ABCD"

if __name__ == "__main__":
    # Parse arguments
    # parsed = ParseHelper(*sys.argv)
    n = int(sys.argv[1])
    grep = sys.argv[2:]

    datafiles = []
    for g in grep:
        datafiles.extend(filter(lambda f: os.path.isfile(f), 
            glob.glob(g, recursive=True)))
    print(datafiles)

    table = dict()
    rows = map(lambda t: ''.join(t),
        itertools.product(alphabet, repeat=n))
    rows = list(rows)

    for dfile in datafiles:
        with open(dfile) as f:
            dlines = f.read().split("\n")

        sample_size = 0
        ngram_counter = dict.fromkeys(rows, 0)

        # count occurrences
        for l in dlines:
            l = l.strip()
            line_size = len(l)-n+1
            sample_size += line_size

            
            for i in range(0, line_size):
                ngram_counter[l[i:i+n]] += 1

        # => frequencies
        for k in rows: 
            ngram_counter[k] /= sample_size 

        table[os.path.basename(dfile)] = ngram_counter

    df = pd.DataFrame.from_dict(table)
    df.to_csv("{}gram.csv".format(n))

    #print(df)


